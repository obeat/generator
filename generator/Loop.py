from pydub import AudioSegment
from pydub.playback import play
from pydub import utils
from pydub import effects
from random import randint
import threading
import array
import math
import copy
from pysndfx import AudioEffectsChain


defaultRate = 44100


class Loop:
    #thread: threading.Thread
    #audio:  AudioSegment

    def __init__(self, **kwargs):
        file = kwargs.get('file')
        cpy = kwargs.get('copy')
        if file:
            self.audio = AudioSegment.from_file(file)
            self.gain(-20)
        elif cpy:
            self.audio = copy.deepcopy(cpy.audio)
        else:
            self.audio = AudioSegment(data=array.array('i'), sample_width=2,
                                      frame_rate=defaultRate, channels=2)
        self.thread = threading.Thread(name='Playing', target=self.playInThread)

    def __add__(self, otherloop):
        self.audio = self.audio + otherloop.audio
        return self

    def mix(self, tomix):
        self.audio = self.audio.overlay(tomix.audio)

    def playInThread(self):
        play(self.audio)

    def play(self):
        if not self.isPlaying():
            self.thread.start()

    def isPlaying(self):
        return self.thread.is_alive()

    def lowPassOnce(self, cmin, cmax):
        original = self.audio.get_array_of_samples()
        filteredArray = array.array(self.audio.array_type, original)
        frame_count = int(self.audio.frame_count())
        last_val = [0] * self.audio.channels
        for i in range(self.audio.channels):
            last_val[i] = filteredArray[i] = original[i]
        coef = (cmax - cmin) / frame_count
        for i in range(1, frame_count):
            for j in range(self.audio.channels):
                alpha = (1.0 / self.audio.frame_rate) / ((1.0 / (cmin * 2 * math.pi))
                                                         + (1.0 / self.audio.frame_rate))
                offset = (i * self.audio.channels) + j
                last_val[j] = last_val[j] + (alpha * (original[offset] - last_val[j]))
                filteredArray[offset] = int(last_val[j])
            cmin += coef
        self.audio = AudioSegment(data=filteredArray, sample_width=2,
                                  frame_rate=self.audio.frame_rate, channels=2)

    def lowPass(self, cmin, cmax):
        self.lowPassOnce(cmin, cmax)
        self.lowPassOnce(cmin, cmax)
        self.lowPassOnce(cmin, cmax)
        self.lowPassOnce(cmin, cmax)

    def lowPassTest(self, cmin, cmax):
        fx = (AudioEffectsChain().highshelf())
        original = self.audio.get_array_of_samples()
        print("type --------------------------------", type(original))
        fx(original)

    def highPass(self, cmin, cmax):
        self.highPassOnce(cmin, cmax)
        self.highPassOnce(cmin, cmax)
        self.highPassOnce(cmin, cmax)
        self.highPassOnce(cmin, cmax)

    def highPassOnce(self, cmin, cmax):
        minval, maxval = utils.get_min_max_value(self.audio.sample_width * 8)
        original = self.audio.get_array_of_samples()
        filteredArray = array.array(self.audio.array_type, original)
        frame_count = int(self.audio.frame_count())
        last_val = [0] * self.audio.channels
        for i in range(self.audio.channels):
            last_val[i] = filteredArray[i] = original[i]
        coef = (cmax - cmin) / frame_count
        for i in range(1, frame_count):
            for j in range(self.audio.channels):
                alpha = (1.0 / (cmin * 2 * math.pi)) / ((1.0 / (cmin * 2 * math.pi))
                                                        + (1.0 / self.audio.frame_rate))
                offset = (i * self.audio.channels) + j
                offset_minus_1 = ((i - 1) * self.audio.channels) + j

                last_val[j] = alpha * (last_val[j] + original[offset] - original[offset_minus_1])
                filteredArray[offset] = int(min(max(last_val[j], minval), maxval))
            cmin += coef
        self.audio = AudioSegment(data=filteredArray, sample_width=2,
                                  frame_rate=self.audio.frame_rate,
                                  channels=self.audio.channels)

    def bandPass(self, cmin, cmax, bandWitdth):
        self.lowPass(cmin, cmax)
        self.highPass(cmin + bandWitdth, cmax + bandWitdth)

    def save(self, path):
        self.audio.export(path, format="flac")

    def normalize(self):
        self.audio = effects.normalize(self.audio)

    def gain(self, gain):
        self.audio = self.audio.apply_gain(gain)

    def whiteNoise(self, time):
        tmp = array.array(self.audio.array_type)
        total = int(float(float(self.audio.frame_rate) * float(time))
                    * float(self.audio.channels))
        for i in range(0, total):
            tmp.append(randint(-300, 300))
        self.audio = AudioSegment(data=tmp, sample_width=2,
                                  frame_rate=self.audio.frame_rate,
                                  channels=self.audio.channels)

    def duration(self):
        return float(float(len(self.audio)) / 1000.0)

    def cutEnd(self, time):
        original = self.audio.get_array_of_samples()
        size = int(self.audio.frame_rate * self.audio.channels * time)
        original = original[-size:]
        if len(original) % 4 != 0:
            for i in range(0, len(original) % 4):
                original.append(0)
        self.audio = AudioSegment(data=original, sample_width=2,
                                  frame_rate=self.audio.frame_rate,
                                  channels=self.audio.channels)        


def demo():
    kick = Loop(file="data/House/Kick/kick1.flac")
    snare = Loop(file="data/House/snare/snare1.flac")
    hat = Loop(file="data/House/Hat/hat1.flac")
    bass = Loop(file="data/House/bass/bass1.flac")
    synth = Loop(file="data/House/synth/piano1.flac")

    song = Loop(copy=synth)
    song2 = Loop(copy=song)
    song2.mix(hat)
    song2.mix(snare)

    song.lowPass(1, 8000)
    song2.highPass(1, 2000)

    song += song2

    sweepUp = Loop()
    sweepUp.whiteNoise(song.duration() * 2)
    sweepUp.bandPass(1, 4000, 20)

    song.mix(sweepUp)

    kick.mix(snare)
    kick.mix(hat)
    kick.mix(bass)
    kick.mix(synth)

    kick += kick

    song += kick

    song.normalize()
    song.play()
