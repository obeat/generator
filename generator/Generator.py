#!/bin/python3

from generator.Loop import Loop
from math import fmod
from generator.File import listDir
from generator.Request import GetSample
from random import randint
import os
import sys
import json
from pydub.playback import play

basePath = os.path.dirname(os.path.abspath(sys.argv[0])) + "/data/"


class Generator:
    style: {}
    landMarks: []
    duration: int
    index: int
    instruments: []
    actualLoop: []

    def __init__(self, landmarks, duration, style, instrument):
        self.styleName = style
        self.style = {}
        self.landMarks = landmarks
        self.duration = duration
        self.instruments = []
        self.actualLoop = []
        data = json.loads(instrument)
        for instru in data:
            tmp = {}
            for i in range(int(data[instru])):
                gs = GetSample(self.styleName, instru, i)
                tmp[str(i)] = gs.getsample()
            self.style[instru] = tmp

    def setInstruments(self, nb):
        if nb == -1:
            i = 0
            for inst, val in self.style.items():
                rnd = randint(0, len(val) - 1)
                self.instruments.append(rnd)
                key = list(val.keys())[rnd]
                self.actualLoop.append(val[key])
                i += 1
        else:
            toChange = []
            while len(toChange) < nb:
                rnd = randint(0, len(self.style) - 1)
                if rnd in toChange:
                    continue
                toChange.append(rnd)
                old = self.instruments[rnd]
                key = list(self.style.keys())[rnd]
                size = len(self.style[key])
                while old == self.instruments[rnd]:
                    self.instruments[rnd] = randint(0, size - 1)
                key2 = list(self.style[key].keys())[self.instruments[rnd]]
                self.actualLoop[rnd] = self.style[key][key2]

    def nextLoop(self, cmin, cmax):
        test = Loop(copy=self.actualLoop[0])
        for i in range(1, len(self.actualLoop)):
            test.mix(self.actualLoop[i])
        if cmin != -1:
            test.lowPass(cmin, cmax)
        return test

    def genOneSong(self, lm, dur):
        self.setInstruments(-1)
        beg = Loop(file=basePath + self.styleName + '/' + 'beg.flac')
        duration = beg.duration()
        nbLoop = int((float(dur) - fmod(lm, duration)) / duration)
        beg.cutEnd(float(fmod(lm, duration)))
        beg.lowPass(1000, 1000)
        landIndex = int((float(lm) - float(fmod(lm, duration))) / float(duration))
        for i in range(0, nbLoop):
            if i < landIndex and i == 0:
                beg += self.nextLoop(1, 6000 / landIndex)
            elif i < landIndex:
                beg += self.nextLoop((6000 / landIndex) * i,
                                     (6000 / landIndex) * (i + 1))
            else:
                beg += self.nextLoop(-1, -1)
            self.setInstruments(randint(2, 3))
        end = Loop(file=basePath + self.styleName + '/' + 'end.flac')
        end.cutEnd(fmod(dur - fmod(lm, duration), duration))
        beg += end
        return beg

    def generate(self):
        song = self.genOneSong(self.landMarks[0], self.duration)
        return song

# Exemple de JSON envoyé en parametre
# {
#   "kick": 5,
#   "snare": 4,
#   "hat": 5
# }
# Exemple /getSample?style=House&instrument=Kick&loop=1
# reponse : fichier audio converti en base64
# {
#   "style": "House",
#   "instrument": "Kick",
#   "loop": 1
# }

# python3 generator.py {durées sec} {style} {Json instrument style}


gen = Generator([20], int(sys.argv[1]), sys.argv[2], sys.argv[3])
gen.generate().save('test.flac')



