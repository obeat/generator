#!/bin/python3

from __future__ import print_function
from random import randint
import os
import sys
import subprocess
import cv2

from Generator import Generator
from Loop import Loop

import scenedetect
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
from scenedetect.frame_timecode import FrameTimecode
from scenedetect.detectors import ContentDetector

def detectScene(vid):
    video_manager = VideoManager([vid])
    v = cv2.VideoCapture(vid)
    v.set(cv2.CAP_PROP_POS_AVI_RATIO,1)
    dur = v.get(cv2.CAP_PROP_POS_MSEC) / 1000
    scene_manager = SceneManager()
    scene_manager.add_detector(ContentDetector())
    base_timecode = video_manager.get_base_timecode()
    start_time = base_timecode + 16.0
    end_time = base_timecode + 60.0
    try:
        video_manager.set_duration(start_time=start_time, end_time=end_time)
        video_manager.set_downscale_factor()
        video_manager.start()
        scene_manager.detect_scenes(video_manager)
        scene_list = scene_manager.get_scene_list(base_timecode)
        filt = []
        for i, scene in enumerate(scene_list):
            num = float(scene[1].get_timecode().split(':')[2])
            + (float(scene[1].get_timecode().split(':')[1]) * 60)
#            if num > 16 and num < 60:
            filt.append(num)
#            elif num >= 60:
#                break
        filt = filt[:len(filt) - 5]
#        print(float(scene_list[len(scene_list) - 1][1].get_timecode().split(':')[1]) * 60
#        + float(scene_list[len(scene_list) - 1][1].get_timecode().split(':')[2]))
#        dur = float(float(scene_list[len(scene_list) - 1][1].get_timecode().split(':')[1]) * 60
#        + float(scene_list[len(scene_list) - 1][1].get_timecode().split(':')[2]))
#        print("test--------------------" + str(dur))
        res = []
        if len(filt) == 0:
            return [0, 0]
        res.append(dur)
        print(len(filt))
        res.append(filt[randint(0, len(filt) - 1)])
        return res
    finally:
        video_manager.release()


result = detectScene(sys.argv[1])
if result[0] == 0:
    print("No cut detected")
    exit(1)

gen = Generator([result[1]], result[0])
music = gen.generate()
music.normalize()
music.save(sys.argv[2])
#print("./generator/OBeat " + " -s " + sys.argv[2], " -f " + sys.argv[3] + " -l "
#      + str(result[1]) + " -d " + str(result[0]))
#subprocess.run(["./generator/OBeat", "-s", sys.argv[2], "-f", sys.argv[3], "-l", str(result[1]), "-d", str(result[0])])
