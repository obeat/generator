from os import walk
import base64
import os


def listDir(path, file, directory):
    ret = []
    for (dirpath, dirnames, filenames) in walk(path):
        if file:
            ret.extend(filenames)
        if directory:
            ret.extend(dirnames)
        break
    return ret


def base64tofile(file64, id):
    try:
        file64 = file64.decode("utf-8")
        file = base64.b64decode(file64)
        with open(str(id) + ".flac", "wb") as f:
            f.write(file)
            f.close()
    except Exception as e:
        print(str(e))


def deletefile(id):
    os.remove(str(id) + ".flac")
