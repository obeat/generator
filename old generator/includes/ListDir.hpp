/*
** EPITECH PROJECT, 2021
** ListDir.cpp
** File description:
** ListDir
*/

#ifndef LISTDIR_HPP
#define LISTDIR_HPP


#include <vector>
#include <string>

class ListDir {
public:
	enum selection
	{
		ALL,
		ONLYFILES,
		ONLYFOLDERS,
	};
	typedef enum selection selection;

	static std::vector<std::string> list(const std::string &, selection sel = ALL);
};


#endif /* !LISTDIR_HPP */
