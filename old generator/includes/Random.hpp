#ifndef RANDOM_H_
#define RANDOM_H_

#include <iostream>
#include <random>

class Random
{
public:
	static std::size_t rand(std::size_t, std::size_t);
};

#endif
