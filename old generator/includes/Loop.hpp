#ifndef SOUNDBUFFEREDDIT_H_
#define SOUNDBUFFEREDDIT_H_

#include <SFML/Audio.hpp>
#include <iostream>
#include "filt.hpp"
#include "ListDir.hpp"

using namespace sf;

class Loop
{
public:
	enum cutPos
	{
		BEGINNING,
		END
	};
	typedef enum cutPos cutPos;

	Loop();
	Loop(const std::string &, const std::string &);
	Loop(const std::string &);
	Loop(const Loop &);
	~Loop();
	void mix(const Loop &);
	void concat(const Loop &);
	void reduce();
	double getDuration() const;
	const std::vector<std::string> getFlacs() const;
	const SoundBuffer *getBuffer() const;
	void play() const;
	void stop() const;
	const std::string getPath() const;
	const std::size_t getId() const;
	bool saveToFile(const std::string &) const;
	bool change();
	void applyChange();
	void lowPassFilter(double, double);
	void highPassFilter(double, double);
	void createVoidSound(unsigned int, unsigned int, double);
	void createBlankSound(unsigned int, unsigned int, double);
	bool isPlaying();
	void cut(double duration, cutPos position = BEGINNING);
private:
	sf::SoundBuffer *_next;
    std::string _path;
	std::vector<std::string> _flacs;
    SoundBuffer *_buffer;
    Sound *_sound;
    std::size_t _id;
};

#endif
