#ifndef ARGSMANAGER_H_
#define ARGSMANAGER_H_

#include <boost/program_options.hpp>
#include <iostream>

class ArgsManager
{
public:
	static void manageArgs(int ac, char **av, std::string &style,
		std::string &flac, std::string &lm, std::string &dur);
};

#endif
