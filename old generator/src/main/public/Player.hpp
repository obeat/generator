#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Device.hpp"

class Player : public Device {
public:
    Player(const std::string &);
    void play();
    static void stopPlaying(int);
    void selectPatterns(std::size_t, std::size_t);
private:
    std::map<std::string, Loop *> _tmpMusic;
};


#endif /* !PLAYER_HPP */
