#ifndef DEVICE_HPP
#define DEVICE_HPP

#include <vector>
#include <map>
#include "Loop.hpp"
#include "Random.hpp"

class Device {
public:
    explicit Device(const std::string &);
    ~Device();

protected:
    void selectInsturments(std::size_t, std::size_t);
    void changeInstruments();
    std::vector<std::string> _instruments;
    std::vector<int> _changes;
    std::map<std::string, Loop *> _music;
    std::string _style;
};


#endif /* !DEVICE_HPP */
