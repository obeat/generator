#ifndef GENERATOR_HPP
#define GENERATOR_HPP

#include "Loop.hpp"
#include <signal.h>
#include <map>
#include <Random.hpp>
#include "ListDir.hpp"
#include "Device.hpp"

class Generator : public Device {
public:
    explicit Generator(const std::string &style);
    void setFilter(int &nbLoop, Loop &result, double landmark, double loopDuration);
    void save(const std::string &name);
    void save(const std::string &name, const std::string &lm, const std::string &dur);
};


#endif
