#include "ArgsManager.hpp"

using namespace std;
namespace po = boost::program_options;

void ArgsManager::manageArgs(int ac, char **av, string &style, string &flac,
		string &lm, string &dur
)
{
	po::options_description desc("Allowed options");
	desc.add_options()("help,h", "Produce help message")("style,s",
			po::value<string>()->implicit_value(""),
			"REQUIRED: Set the music style (house, reggae, ...)")("flac,f",
			po::value<string>()->implicit_value(""),
			"OPTIONAL: Save the song in the file {arg}.flac")("lm,l",
			po::value<string>()->implicit_value(""),
			"OPTIONAL: Set a landmark to create an effect (second)")("dur,d",
			po::value<string>()->implicit_value(""),
			"OPTIONAL: Set the total sound duration (second)");
	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);
	if (vm.count("help")) {
		cout << desc << endl;
		exit(0);
	}
	if (vm.count("style") && !(vm["style"].as<string>().empty()))
		style = vm["style"].as<string>();
	else {
		cerr << desc << endl;
		throw logic_error(string("--style is required"));
	}
	if (vm.count("flac")) {
		if (vm["flac"].as<string>().empty()) {
			cerr << desc << endl;
			throw logic_error(string("--flac {fileName.flac}"));
		}
		flac = vm["flac"].as<string>();
	}
	if (vm.count("lm")) {
		if (vm["lm"].as<string>().empty()) {
			cerr << desc << endl;
			throw logic_error(string("--lm {second}"));
		}
		if (!vm.count("dur")) {
			cerr << desc << endl;
			throw logic_error(string("--dur {second} is required with --lm"));
		}
		lm = vm["lm"].as<string>();
	}
	if (vm.count("dur")) {
		if (vm["dur"].as<string>().empty()) {
			cerr << desc << endl;
			throw logic_error(string("--dur {second}"));
		}
		dur = vm["dur"].as<string>();
	}
}
