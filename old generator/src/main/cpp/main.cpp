#include <Player.hpp>
#include "Generator.hpp"
#include "ArgsManager.hpp"

using namespace std;

int main(int ac, char **av)
{
	try {
		std::string style, flac, lm, dur;
		ArgsManager::manageArgs(ac, av, style, flac, lm, dur);
		std::cout << "dur = " << dur << std::endl;
		Generator gen = Generator(style);
		if (flac.empty()) {
		    Player player = Player(style);
		    player.play();
		} else if (lm.empty())
			gen.save(flac);
        else
			gen.save(flac, lm, dur);
	} catch (exception &e) {
		cerr << e.what() << std::endl;
		return 1;
	}
	return 0;
}
