/*
** EPITECH PROJECT, 2021
** ListDir.cpp
** File description:
** ListDir
*/

#include "ListDir.hpp"
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <iostream>
#include <cstring>

std::vector<std::string> ListDir::list(const std::string &path, selection sel)
{
	DIR *dir = opendir(path.c_str());
	std::vector<std::string> tab;

	struct dirent *ent;
	if (!dir)
		throw std::logic_error("Can't find: " + path);
	while ((ent = readdir(dir)) != NULL) {
		if (strcmp(ent->d_name, ".") != 0  && strcmp(ent->d_name, "..") != 0) {
			struct stat s;
			if (stat((path + '/' + ent->d_name).c_str(), &s) == 0) {
				if(s.st_mode & S_IFDIR && sel != ListDir::ONLYFILES)
					tab.emplace_back(ent->d_name);
				else if(s.st_mode & S_IFREG && sel != ListDir::ONLYFOLDERS)
					tab.emplace_back(ent->d_name);
				else if (sel == ListDir::ALL)
					tab.emplace_back(ent->d_name);
				
			}
		}
	}
	closedir(dir);
	return tab;
}
