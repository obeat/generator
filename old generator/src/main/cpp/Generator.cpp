#include <thread>
#include "Generator.hpp"

using namespace std;

Generator::Generator(const string &style) : Device(style)
{
}

void Generator::save(const std::string &save)
{
	Loop result;

	for (std::size_t i = 0; i < 8; i++) {
		std::cout << "Loop " << i + 1 << std::endl;
		Loop *loop = new Loop(*(_music.begin()->second));
		loop->reduce();
		for (auto it = _music.begin()++; it != _music.end(); it++)
			loop->mix(*(it->second));
		if (i == 0)
			loop->lowPassFilter(10, 6000);
		else if (i == 7)
			loop->lowPassFilter(6000, 10);
		result.concat(*loop);
		delete loop;
		this->selectInsturments(2, 3);
		this->changeInstruments();
	}
	std::cout << (result.saveToFile(save) ? "Save succeed" : "Save failed")
		<< std::endl;
}

void Generator::setFilter(int &nbLoop, Loop &result, double landmark,
	double loopDuration)
{
	int landLength = (int)((landmark - std::fmod(landmark, loopDuration)) /
		loopDuration);
	std::cout << "land = " << landLength << std::endl;
	int raise = landLength == 0 ? 0 : 5900 / landLength;
	int level = 100;
	for (std::size_t i = 0; i < nbLoop; i++) {
		std::cout << "Loop " << i + 1 << std::endl;
		Loop *loop = new Loop(*(_music.begin()->second));
		loop->reduce();
		for (auto it = _music.begin()++; it != _music.end(); it++)
			loop->mix(*(it->second));
		/*if (i == nbLoop - 1)
			loop->lowPassFilter(6000, 100);
		else */
		if (i < landLength)
			loop->lowPassFilter(level, level + raise);
		level += raise;
		result.concat(*loop);
		delete loop;
		this->selectInsturments(2, 3);
		this->changeInstruments();
	}
}

void Generator::save(const std::string &save, const std::string &lm, const std::string &dur)
{
	Loop result("data/" + _style + "/beg.flac");
	std::cout << lm << std::endl;

	std::string::size_type sz;
	double landmark = std::stod(lm, &sz);
	double loopDuration = _music.begin()->second->getDuration();
	int nbLoop = (int)((std::stof(dur) - std::fmod(landmark, loopDuration)) /
		loopDuration);
	std::cout << "nbLoop = " << nbLoop << std::endl;
	result.cut(std::fmod(landmark, loopDuration), Loop::END);
	result.reduce();
	result.lowPassFilter(100, 100);
	/*result.createVoidSound(
		_music.begin()->second->getBuffer()->getChannelCount(),
		_music.begin()->second->getBuffer()->getSampleRate(),
		std::fmod(landmark, _music.begin()->second->getDuration()));*/
	setFilter(nbLoop, result, landmark, loopDuration);
	if (std::fmod(std::stof(dur) - std::fmod(landmark, loopDuration), loopDuration)) {
		Loop end("data/" + _style + "/end.flac");
		end.cut(std::fmod(std::stof(dur) - std::fmod(landmark, loopDuration),
				loopDuration), Loop::BEGINNING);
		end.reduce();
		end.lowPassFilter(100, 100);
		result.concat(end);
	}
	std::cout << (result.saveToFile(save) ? "Save succeed" : "Save failed")
		<< std::endl;
}