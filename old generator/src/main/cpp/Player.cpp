
#include <csignal>
#include <thread>
#include "Player.hpp"

bool playLoop;

void Player::stopPlaying(int sig)
{
	playLoop = false;
}

Player::Player(const std::string &style)
    : Device(style)
{}

void Player::play()
{
    playLoop = true;
    signal(SIGINT, stopPlaying);
    while (playLoop) {
        std::thread changer(&Player::selectInsturments, this, 1, 3);
        for (auto it = this->_music.begin(); it != this->_music.end(); it++) {
            it->second->stop();
            it->second->play();
        }
        while (playLoop && this->_music.begin()->second->isPlaying());
        changer.join();
        this->changeInstruments();
    }
}

