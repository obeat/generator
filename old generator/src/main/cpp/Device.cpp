
#include <ListDir.hpp>
#include "Device.hpp"

Device::Device(const std::string &style)
{
    this->_instruments = ListDir::list("./data/" + style + "/", ListDir::ONLYFOLDERS);
    for (auto &_instru : _instruments) {
        this->_music[_instru] = new Loop(style, _instru);
    }
    this->_style = style;
}

Device::~Device()
{
    for (auto it = this->_music.begin(); it != this->_music.end(); it++)
        delete it->second;
}

void Device::selectInsturments(std::size_t min, std::size_t max)
{
    auto randMax = Random::rand(min, max);

    while (_changes.size() < randMax) {
        auto rnd = Random::rand(0, _music.size() - 1);
        if (std::find(_changes.begin(), _changes.end(), rnd) != _changes.end())
            continue ;
        if (this->_music[this->_instruments[rnd]]->change())
            _changes.push_back((int)(rnd));
        else
            randMax--;
    }
}

void Device::changeInstruments()
{
    for (int &_change : this->_changes) {
        this->_music[this->_instruments[_change]]->applyChange();
    }
    this->_changes.clear();
}


