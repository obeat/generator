#include "Random.hpp"

std::size_t Random::rand(std::size_t min, std::size_t max)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int>
		distribution((int)min, (int)max);
	return (distribution(mt));
}
