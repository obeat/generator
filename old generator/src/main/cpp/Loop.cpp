#include <Random.hpp>
#include "Loop.hpp"

Loop::Loop()
{
	this->_buffer = new SoundBuffer;
	this->_sound = new Sound(*this->_buffer);
}

Loop::Loop(const std::string &path)
{
	this->_path = path;
	this->_buffer = new SoundBuffer;
	this->_buffer->loadFromFile(path);
	this->_sound = new Sound(*(this->_buffer));
}

Loop::Loop(const std::string &style, const std::string &instru)
{
	this->_path = "./data/" + style + "/" + instru + "/";
	this->_flacs = ListDir::list(this->_path, ListDir::ONLYFILES);
	this->_buffer = new SoundBuffer;
	this->_id = Random::rand(0, this->_flacs.size() - 1);
	this->_buffer->loadFromFile(this->_path + this->_flacs[this->_id]);
    this->_sound = new Sound(*this->_buffer);
}

Loop::Loop(const Loop &cpy)
{
	this->_path = cpy.getPath();
	this->_id = cpy.getId();
	this->_flacs = cpy.getFlacs();
	this->_buffer = new SoundBuffer(*(cpy.getBuffer()));
    this->_sound = new Sound(*this->_buffer);
}

Loop::~Loop()
{
    delete this->_buffer;
    delete this->_sound;
}

void Loop::mix(const Loop &add)
{
	unsigned int channels = std::max(this->_buffer->getChannelCount(), add.getBuffer()->getChannelCount());
	unsigned int rate     = std::max(this->_buffer->getSampleRate(), add.getBuffer()->getSampleRate());
	std::size_t  size     = std::max(this->_buffer->getSampleCount(), add.getBuffer()->getSampleCount());
	std::vector<sf::Int16> samples(size, 0);

	for (std::size_t i = 0; i < size; ++i)
	{
		sf::Int16 b1 = 0, b2 = 0;
		if (i < this->_buffer->getSampleCount())
			b1 = this->_buffer->getSamples()[i];
		if (i < add.getBuffer()->getSampleCount())
			b2 = (sf::Int16)(add.getBuffer()->getSamples()[i] / 10);
		if (b1 < 0 && b2 < 0)
			samples[i] = (b1 + b2) - static_cast<sf::Int16>((b1 * b2) / INT16_MIN);
		else if (b1 > 0 && b2 > 0)
			samples[i] = (b1 + b2) - static_cast<sf::Int16>((b1 * b2) / INT16_MAX);
		else
			samples[i] = b1 + b2;
	}
	this->_buffer->loadFromSamples(samples.data(), samples.size(), channels, rate);
}

void Loop::concat(const Loop &add)
{
	unsigned int channels = std::max(this->_buffer->getChannelCount(), add.getBuffer()->getChannelCount());
	unsigned int rate     = std::max(this->_buffer->getSampleRate(), add.getBuffer()->getSampleRate());
	std::size_t  size     = this->_buffer->getSampleCount() + add.getBuffer()->getSampleCount();
	std::vector<sf::Int16> samples(size, 0);

	for (std::size_t i = 0; i < this->_buffer->getSampleCount(); i++)
		samples[i] = this->_buffer->getSamples()[i];
	for (std::size_t i = 0; i < add.getBuffer()->getSampleCount(); i++)
		samples[i + this->_buffer->getSampleCount()] = add.getBuffer()->getSamples()[i];
	this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}

void Loop::reduce()
{
	unsigned int channels = this->_buffer->getChannelCount();
	unsigned int rate     = this->_buffer->getSampleRate();
	std::size_t  size     = this->_buffer->getSampleCount();
	std::vector<sf::Int16> samples(size, 0);

	for (std::size_t i = 0; i < size; i++) {
		samples[i] = (short)(this->_buffer->getSamples()[i] / 10);
	}
	this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}


void Loop::lowPassFilter(double init, double end)
{
	unsigned int channels = this->_buffer->getChannelCount();
	unsigned int rate     = this->_buffer->getSampleRate();
	std::size_t  size     = this->_buffer->getSampleCount();
	std::vector<sf::Int16> samples(size, 0);
	double delta = (end - init) / size;
	Filter my_filter(LPF, 100, rate, init);

	if (my_filter.get_error_flag() < 0)
		throw std::logic_error("Low pass filter init failed");
	std::cout << delta << " " << init << std::endl;
	for (std::size_t i = 0; i < size; i++) {
		if (i % channels == 0)
			my_filter.setFx(init);
		int test = (int)(my_filter.do_sample((double)this->_buffer->getSamples()[i]));
		samples[i] = (short)test;
		if (init > 0.9)
			init += delta;
	}
	std::cout << delta << " " << init << std::endl;
	this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}

void Loop::highPassFilter(double init, double end)
{
	unsigned int channels = this->_buffer->getChannelCount();
	unsigned int rate     = this->_buffer->getSampleRate();
	std::size_t  size     = this->_buffer->getSampleCount();
	std::vector<sf::Int16> samples(size, 0);
	double delta = (end - init) / size;
	Filter my_filter(HPF, 100, rate, init);

	if (my_filter.get_error_flag() < 0)
		throw std::logic_error("Low pass filter init failed");
	std::cout << delta << " " << init << std::endl;
	for (std::size_t i = 0; i < size; i++) {
		my_filter.setFx(init);
		int test = (int)my_filter.do_sample((double)this->_buffer->getSamples()[i]);
		samples[i] = (short)test;
		if (init > 0.9)
			init += delta;
	}
	std::cout << delta << " " << init << std::endl;
	this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}


void Loop::createVoidSound(unsigned int channels, unsigned int rate, double second)
{
    auto size     = (std::size_t)(rate * channels * second);
    std::vector<sf::Int16> samples(size, 0);
    this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}

void Loop::createBlankSound(unsigned int channels, unsigned int rate, double second)
{
    auto size     = (std::size_t)(rate * channels * second);
    std::vector<sf::Int16> samples(size, 0);
    for (std::size_t i = 0; i < size; i++)
        samples[i] = (short)(Random::rand(0, 1000) - 500);
    this->_buffer->loadFromSamples(samples.data(), size, channels, rate);
}

void Loop::cut(double duration, cutPos position)
{
	unsigned int channels = this->_buffer->getChannelCount();
	unsigned int rate     = this->_buffer->getSampleRate();
	std::size_t  size     = this->_buffer->getSampleCount();
	std::size_t newSize   = rate * channels * duration;
	std::vector<sf::Int16> samples(newSize, 0);

	for (std::size_t i = 0; position == Loop::BEGINNING && i < newSize; i++)
		samples[i] = this->_buffer->getSamples()[i];
	for (std::size_t i = 0; position == Loop::END && i < newSize; i++)
		samples[i] = this->_buffer->getSamples()[size - newSize + i];
	this->_buffer->loadFromSamples(samples.data(), newSize, channels, rate);
}


double Loop::getDuration() const
{
    unsigned int channels = this->_buffer->getChannelCount();
    unsigned int rate     = this->_buffer->getSampleRate();
    std::size_t  size     = this->_buffer->getSampleCount();
	return (double)this->_buffer->getSampleCount() / ((double)channels * (double)rate);
}

const SoundBuffer *Loop::getBuffer() const
{
	return this->_buffer;
}

const std::vector<std::string> Loop::getFlacs() const
{
	return this->_flacs;
}

bool Loop::saveToFile(const std::string &name) const
{
	return this->_buffer->saveToFile(name);
}

bool Loop::change()
{
	if (this->_flacs.size() == 1)
		return false;
	std::size_t rnd = Random::rand(0, this->_flacs.size() - 1);
	while (rnd == this->_id)
		rnd = Random::rand(0, this->_flacs.size() - 1);
	_next = new sf::SoundBuffer;
	_next->loadFromFile(this->_path + this->_flacs[rnd]);
	std::cout << this->_path + this->_flacs[this->_id] << " change to "
	<< this->_path + this->_flacs[rnd] << std::endl;
	this->_id = rnd;
	return true;
}

const std::size_t Loop::getId() const
{
	return this->_id;
}

const std::string Loop::getPath() const
{
	return this->_path;
}

void Loop::play() const
{
    this->_sound->play();
}

void Loop::stop() const
{
    this->_sound->stop();
}

bool Loop::isPlaying()
{
    return this->_sound->getStatus() == sf::SoundSource::Status::Playing;
}

void Loop::applyChange()
{
    this->_sound->stop();
    delete this->_buffer;
    this->_sound->setBuffer(*(this->_next));
    this->_buffer = _next;
    std::cout << "change apply!" << std::endl;
}