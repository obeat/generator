FROM node:10.15.0
WORKDIR /app
######################################################################################################################
################################################# INSTALLING FFMPEG ##################################################
RUN apt-get update ; apt-get install -y git build-essential gcc make yasm autoconf automake cmake libtool checkinstall libmp3lame-dev pkg-config libunwind-dev zlib1g-dev libssl-dev

RUN apt-get update \
    && apt-get clean \
    && apt-get install -y --no-install-recommends libc6-dev libgdiplus wget software-properties-common

#RUN RUN apt-add-repository ppa:git-core/ppa && apt-get update && apt-get install -y git

RUN wget https://www.ffmpeg.org/releases/ffmpeg-4.0.2.tar.gz
RUN tar -xzf ffmpeg-4.0.2.tar.gz; rm -r ffmpeg-4.0.2.tar.gz
RUN cd ./ffmpeg-4.0.2; ./configure --enable-gpl --enable-libmp3lame --enable-decoder=mjpeg,png --enable-encoder=png --enable-openssl --enable-nonfree


RUN cd ./ffmpeg-4.0.2; make
RUN  cd ./ffmpeg-4.0.2; make install
######################################################################################################################
######################################################################################################################
RUN apt-get update
RUN apt-get install -y ffmpeg
RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN apt-get install -y python-pip
RUN pip3 install --user opencv-python
RUN pip install --user opencv-python
RUN pip3 install --user scenedetect
RUN pip3 install --user pydub
COPY ./server/*.json ./server/
RUN cd server && npm install
COPY . .
CMD cd server && npm start