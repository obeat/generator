var video = document.querySelector('.video');
var markers = document.querySelector('.markers');
var marker = document.querySelector('.marker');
var juice = document.querySelector('.dynamic-bar');
var btn = document.getElementById('play-pause');
var badge = document.getElementById('badge');
var arrow = document.querySelector('.arrow-up');
var audio = document.getElementById('audio');
var bar = document.getElementById('progress');
var tBar = document.getElementById('time-bar');
var prog = 0;
var clicked = false;

document.getElementById('logo').src = "http://" + window.location.host + "/icon";
document.getElementById('poster').poster = "http://" + window.location.host + "/poster";
document.getElementById('video').src = "http://" + window.location.host + "/video";
loadAudio();

function timeBarClick(event) {
    var mTime = event.clientX * video.duration / window.innerWidth;
    video.currentTime = mTime;
    audio.currentTime = mTime;
}

function playPause() {
    if (prog != 0)
        return ;
    if (video.paused) {
        btn.className = 'pause';
        audio.play();
        video.play()
    } else {
        btn.className = 'play';
        audio.pause();
        video.pause()
    }

}

function dynamicBar() {
    var juicePos = video.currentTime / video.duration;

    juice.style.width = juicePos * 100 + "%";
}

function placeMarker(event) {
    var mTime = event.clientX * video.duration / window.innerWidth;
    console.log((Math.round(mTime * 100) / 100).toString().length);
    if ((Math.round(mTime * 100) / 100).toString().length == 5)
        arrow.style.left = "12px";
    else if ((Math.round(mTime * 100) / 100).toString().length == 4)
        arrow.style.left = "10px";
    /*else if ((Math.round(mTime * 100) / 100).toString().length == 4)
        arrow.style.left = "-22px";*/
    marker.style.left = event.clientX - 22 + "px";
    badge.innerHTML = Math.round(mTime * 100) / 100;
}

btn.onclick = playPause;
video.addEventListener('timeupdate', dynamicBar);
markers.onclick = placeMarker;
tBar.onclick = function (event) {
    timeBarClick(event);
    clicked = !clicked;
};
tBar.onmousemove = function (event) {
    if (clicked)
        timeBarClick(event);
}

function loadAudio()
{
    console.log("Loading audio...");
    document.getElementById('audioSrc').src = "http://" + window.location.host + "/audio";
    console.log("Audio loaded");
}

function countProgress()
{
    prog += 100 / 13;
    bar.style.width = prog + '%';
    if (prog < 100)
        setTimeout(countProgress, 1000);
    else{
		if (btn.className = 'play'){
			loadAudio();
			refresh();
		} else
			alert("Veuillez reactualiser la page");
		prog = 0;
	}
}

function allowDrop(event) {
    event.preventDefault();
    //console.log(event.dataTransfer.types);
    //event.dataTransfer.types.contains("application/x-moz-file");
}

function drop(event) {
    //alert('test');
    console.log(event.dataTransfer.getData());
    //var file = event.dataTransfer.mozGetDataAt("application/x-moz-file", 0);
    //console.log(file);
}

function generate() {
    var value;
    var radios = document.getElementsByName("style");
    for (var i = 0; i < radios.length; i++)
        if (radios[i].checked)
            value = radios[i].value;
    var lm = badge.innerHTML.replace(/[^a-zA-Z0-9.]+/g, "");
    var dur = video.duration;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://" + window.location.host + "/generate", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log("success");
        }
    }
    xhr.send(encodeURI("type=" + value + "&landmark=" + lm + "&duration=" + dur));
    prog += 5;
    setTimeout(countProgress, 1000);
}
function refresh()
{
	video.load();
	audio.load();
	video.muted = true;
}
refresh();