var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
var morgan = require('morgan')
var fs = require('fs')

var app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))

var musicPath = "output/music.flac"

app.get('/css', function (req,res) {
    sendFile('text/css', 'design/style.css', res, req);
})

app.get('/script', function(req, res) {
    sendFile('text/javascript', 'design/script.js', res, req);
})

app.get('/', function(req,res){
    res.sendfile('design/page.html');
});

app.get('/video', function(req, res) {
    sendFile('video/mp4', 'design/mp4/movie.mp4', res, req);
});

app.get('/icon', function(req, res) {
   sendFile('image/png', 'design/img/icon.png', res, req);
});

app.get('/poster', function(req, res) {
    sendFile('image/png', 'design/img/poster.png', res, req);
});

app.get('/audio', function(req, res) {
    sendFile('audio', musicPath, res, req)
});

app.post('/generate', function(req, res) {
    const { exec } = require('child_process')
    try {
		fs.unlinkSync(musicPath)
    } catch (err) {
		console.log("No music created yet.")
    }
    console.log("python3 ../generator/videoAnalysis.py design/mp4/movie.mp4 " + musicPath)
    exec("python3 ../generator/videoAnalysis.py design/mp4/movie.mp4 " + musicPath,
	 (err, stdout, stderr) => {
	     if (err) {
		 console.log(err)
		 return ;
	     }
	     console.log(`stdout: ${stdout}`)
	     console.log(`stderr: ${stderr}`)
	 })
    res.sendStatus(200);
});

app.listen(8082, function(){	
});

/**
  * Functions utils
  */


function sendFile(type, path1, res, req) {
    const path = path1;
    const stat = fs.statSync(path)
    const fileSize = stat.size
    const range = req.headers.range

    if (range) {
        const parts = range.replace(/bytes=/, "").split("-")
        const start = parseInt(parts[0], 10)
        const end = parts[1]
            ? parseInt(parts[1], 10)
            : fileSize-1
        const chunksize = (end-start)+1
        const file = fs.createReadStream(path, {start, end})
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': type,
        }

        res.writeHead(206, head);
        file.pipe(res);
    } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': type,
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
    }
}

